import * as SentryClient from '@sentry/react';

import { ErrorType } from '@/types/Common.types';

export type SentryLogParams<P = Record<string, unknown>> = {
  action: string;
  params?: P;
  payload?: ErrorType | Array<ErrorType>;
};

export type SentryContextParams = {
  name: string;
  context: Record<string, unknown>;
};

export default class Sentry {
  client: typeof SentryClient;

  constructor() {
    this.client = SentryClient;
  }

  /**
   * Updates user context information for future events.
   * @param user User context object to be set in the current context. Pass `null` to unset the user.
   */
  setMonitoringUser(user: SentryClient.User): void {
    this.client.setUser(user);
  }

  /**
   * Clears user monitoring context.
   * @returns void
   */
  clearMonitoringUser(): void {
    this.client.setUser(null);
  }

  /**
   * Sets context data with the given name.
   * @param params object containing the name and any kind of data to for the context, which will be normalized
   */
  setMonitoringContext(params: SentryContextParams): void {
    this.client.setContext(params.name, params.context);
  }

  /**
   * Callback to set context information onto the scope.
   * @param callback Callback function that receives Scope.
   */
  setMonitoringScope(callback: () => void): void {
    this.client.configureScope(callback);
  }

  /**
   * Payload validation when its status code is 1005 (Connection Denied).
   * @param payload error payload that has error status code and error message.
   */
  isPayloadErrorDenied(payload: ErrorType | Array<ErrorType>): boolean {
    return (
      (payload instanceof Array &&
        payload.length !== 0 &&
        payload[0] &&
        payload[0].code === 1005) ||
      (!(payload instanceof Array) && payload.code === 1005)
    );
  }

  /**
   * Log a message event and sends it to Sentry.
   * @param config Configuration Object for the Message Event
   */
  log<T>(config: SentryLogParams<T>): void {
    const { action, params, payload } = config;

    if (payload && this.isPayloadErrorDenied(payload)) {
      return;
    }

    this.client.captureMessage(action, {
      level: SentryClient.Severity.Log,
      extra: {
        ...params,
        ...payload,
      },
    });
  }

  /**
   * Capture a warn message event and sends it to Sentry.
   * @param config Configuration Object for the Message Event
   */
  warn<T>(config: SentryLogParams<T>): void {
    const { action, params, payload } = config;

    if (payload && this.isPayloadErrorDenied(payload)) {
      return;
    }

    this.client.captureMessage(action, {
      level: SentryClient.Severity.Warning,
      extra: {
        ...params,
        ...payload,
      },
    });
  }

  /**
   * Capture an error message event and sends it to Sentry.
   * @param config Configuration Object for the Message Event
   */
  error<T>(config: SentryLogParams<T>): void {
    const { action, params, payload } = config;

    if (payload && this.isPayloadErrorDenied(payload)) {
      return;
    }

    this.client.captureMessage(action, {
      level: SentryClient.Severity.Error,
      extra: {
        ...params,
        ...payload,
      },
    });
  }
}
