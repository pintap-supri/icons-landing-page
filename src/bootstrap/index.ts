import Http from '@/bootstrap/Http.bootstrap';
import Sentry from '@/bootstrap/Sentry.bootstrap';

// #endregion

const http = new Http();
const sentry = new Sentry();

export { http, sentry };
