import * as IconOutline from '@pt-solusi-pintap-indonesia/pintap-data-icons/react/outline';
import React, { useMemo, useState } from 'react';

import { SelectOption } from '@/components';
import { Select, Text } from '@/components/atoms';
import { css, styled, tw, tws } from '@/utils/windicss';

const Container = tw.div`w-full p-16 flex flex-col gap-14`;
const IconContainer = tw.div`grid grid-cols-7 gap-8`;
const IconWrapper = tw.div`flex flex-col items-center gap-2`;
const IconDescription = styled(Text.TinyTight)`text-xs`;
const baseIcon = tws`w-4 h-4`;

function App() {
  const [searchSelected, setSearchSelected] = useState<SelectOption[]>([]);

  const iconsEntries = useMemo(
    () =>
      Object.entries(IconOutline).map((data) => ({
        name: data[0],
        Component: data[1],
      })),
    [IconOutline],
  );

  const iconsOptions = useMemo(
    () =>
      iconsEntries.map((data) => ({
        label: data.name,
        value: data.name,
      })),
    [iconsEntries],
  );

  const filteredIcons = useMemo(
    () =>
      iconsEntries.filter((icon) => icon.name.includes(searchSelected[0]?.value ?? '')),
    [iconsEntries, searchSelected, setSearchSelected],
  );

  return (
    <Container>
      <Select
        placeholder="Search icons..."
        options={iconsOptions}
        value={searchSelected}
        onChange={(opt) => setSearchSelected(opt)}
        searchable
      />
      <IconContainer>
        {filteredIcons.map(({ name, Component }, index) => (
          <IconWrapper key={index}>
            <Component className={css([baseIcon])} />
            <IconDescription>{name}</IconDescription>
          </IconWrapper>
        ))}
      </IconContainer>
    </Container>
  );
}

export default App;
