import './index.css';
import 'virtual:windi.css';

import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Route, Router, Switch } from 'wouter';

import { Router as BootstrapRouter } from '@/router/Bootstrap.router';

import { env } from './config/Environment.config';

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<></>}>
      <Router base={env.basePath}>
        <Switch>
          <Route path="/">
            <BootstrapRouter />
          </Route>
          <Route>404, Not Found!</Route>
        </Switch>
      </Router>
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root'),
);
