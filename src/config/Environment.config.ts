export const env = {
  mode: process.env.NODE_ENV,
  baseUrl: process.env.REACT_APP_LANDING_URL || 'http://localhost:3000/',
  basePath: process.env.REACT_APP_BASE_PATH || '/'
};
