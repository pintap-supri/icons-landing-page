import React from 'react';

import { AppRoute } from '@/router';

export const Routes: AppRoute[] = [
  {
    path: '/',
    name: 'home',
    isPrivate: false,
    exact: true,
    RouteComponent: React.lazy(() => import('../views/Home')),
  },
];
