/* eslint-disable no-unused-vars */

export type StyleTypes = string | boolean | undefined;
export type ExtraStyle = StyleTypes | StyleTypes[];
export type CssStyle = ExtraStyle[];

export type StyledInterface<
  T extends keyof JSX.IntrinsicElements | React.JSXElementConstructor<any>,
> = React.FC<
  | React.ComponentProps<T> & {
      css?: CssStyle;
    }
>;
export type TwInterFace<
  T extends keyof JSX.IntrinsicElements | React.JSXElementConstructor<any>,
> = (str: TemplateStringsArray) => StyledInterface<T>;

export type twcProps = {
  [k in keyof React.ReactHTML]: TwInterFace<k>;
};
