/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
import { compact, flattenDeep, uniqWith } from 'lodash';
import React from 'react';

import { allTagsName, regexScanStyle } from './Constants';
import { CssStyle, StyledInterface, twcProps, TwInterFace } from './Types';

/**
 * This helper function is working like twin.macro.
 * In some cases like two or more pseudo classes,
 * you should separate those css like sm:hover:(text-white) not sm:(hover:text-white)
 *
 * @param str Windi classname array that can be used like twin.macro
 * @returns string windi classname
 */
export const css = (str: CssStyle) => {
  // flat the string array if there is array in array.
  const flattenedArr = flattenDeep(str);
  // remove boolean and undefined type in array.
  const filteredArr = compact(flattenedArr) as string[];
  // separate string in order to evaluate each css.
  const splittedArr = filteredArr.map((item) => item.split(' ')).flat();
  // remove any symbol that can interfere the evaluation
  const removedSymbolArr = splittedArr.map((item) => item.replace(regexScanStyle, ''));
  // reverse the array so the last css will be evaluated.
  const reversedArr = removedSymbolArr.reverse();
  // evaluate the array to make it unique. remove same css to make windicss works.
  const uniqueArr = uniqWith(reversedArr, (a: string, b: string) => {
    const arrA: string[] = a.split('-');
    const arrB: string[] = b.split('-');

    const basicColor = ['white', 'black', 'transparent'];
    const regexHasNumeric = /[0-9]/;
    const exceptionalMod1 = ['group'];
    const exceptionalMod2 = ['border'];

    const checkDuplicatedMod = (idx: number) => {
      return (
        Boolean(arrA[idx]) &&
        Boolean(arrB[idx]) &&
        ((basicColor.includes(arrA[idx]) && basicColor.includes(arrB[idx])) ||
          (arrA[idx].length <= 4 && arrB[idx].length <= 4) ||
          (arrB[idx].length > 4 && arrA[idx].length > 4) ||
          (regexHasNumeric.test(a) && regexHasNumeric.test(b)) ||
          (!regexHasNumeric.test(a) && !regexHasNumeric.test(b))) &&
        arrA[0] === arrB[0]
      );
    };

    const isDuplicatedMod = exceptionalMod1.includes(arrA[0])
      ? exceptionalMod2.includes(arrA[1])
        ? checkDuplicatedMod(2) && checkDuplicatedMod(3)
        : checkDuplicatedMod(1) && checkDuplicatedMod(2)
      : checkDuplicatedMod(1);

    return isDuplicatedMod;
  });
  // join array to be used in classname props
  const selectors = uniqueArr.join(' ');
  return selectors;
};

export const styled =
  (Comp: StyledInterface<any>): TwInterFace<any> =>
  (str: TemplateStringsArray): StyledInterface<any> => {
    return function StyledCont({ children, css: cssProps, className, ...props }) {
      return (
        <Comp
          className={css([str.toString(), className, css(cssProps ?? [])])}
          {...props}
        >
          {children}
        </Comp>
      );
    };
  };

export const tws = (str: TemplateStringsArray) => str.toString();

/**
 * This helper function is working like styled emotion.
 * it creates react element with defined css literal.
 * @param str Windi literal that can be used like twin.macro
 * @returns JSX.Element
 */
export const tw: twcProps = Object.fromEntries(
  new Map(
    allTagsName.map((tag) => {
      const twComp = (
        str: TemplateStringsArray,
      ): React.ForwardRefExoticComponent<
        React.RefAttributes<{}> & { css: CssStyle; className: string }
      > => {
        return React.forwardRef(function Comp(
          { children, css: cssProps, className, ...props },
          ref,
        ) {
          return React.createElement(
            tag,
            {
              ...props,
              className: css([str.toString(), className, css(cssProps ?? [])]),
              ref,
            },
            children,
          );
        });
      };
      return [tag, twComp];
    }),
  ),
) as unknown as twcProps;
