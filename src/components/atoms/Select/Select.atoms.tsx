// #region IMPORTS
import Fuse from 'fuse.js';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { ChevronDown, ChevronUp, Loader2, X } from 'tabler-icons-react';
import { useDebounce, useOnClickOutside } from 'usehooks-ts';

import { styled, tw, tws } from '@/utils/windicss';

import { SelectOption as SelectOptionType } from '../../';
// #endregion IMPORTS

// #region STYLED COMPONENTS
const Wrapper = tw.div`relative`;
const Container = tw.div`relative p-2 w-64 border border-info-400 rounded-xl flex flex-wrap gap-1`;
const SelectInputContainer = tw.div`flex-1 min-w-24 flex items-center gap-2`;
const SelectInput = tw.input`w-full outline-none`;
const SelectContainer = tw.div`absolute left-0 -bottom-2 transform translate-y-full w-full max-h-32 flex flex-col border border-primary-500 rounded-xl overflow-auto`;
const SelectOption = tw.button`flex items-start p-2 bg-white border-b border-b-info-100 last:(border-b-0) cursor-pointer hover:(bg-info-50) focus:(outline-none bg-info-50)`;
const TagContainer = tw.div`flex flex-wrap gap-1`;
const Tag = tw.button`py-1 pl-2 pr-1 flex items-center gap-1 border bg-white border-info-500 text-xs text-info-500 rounded-md cursor-pointer hover:(bg-error-100 border-error-500 text-error-500) focus:(outline-none bg-error-100 border-error-500 text-error-500)`;

const CollapseIcon = styled(ChevronUp)`text-info-200`;
const ExpandIcon = styled(ChevronDown)`text-info-200`;
const CloseIcon = styled(X)`w-4 h-4`;
const LoadingIcon = styled(Loader2)`animate-spin`;
// #endregion STYLED COMPONENTS

type SelectProps = {
  placeholder: string;
  value?: SelectOptionType[];
  options: SelectOptionType[];
  multiple?: boolean;
  searchable?: boolean;
  leftIcon?: JSX.Element;
  rightIcon?: JSX.Element;
  onChange: (data: SelectOptionType[]) => void;
  onSearchChange?: (value: string) => void;
  asyncCallback?: (searchText: string) => Promise<SelectOptionType[]>;
};

const Select: React.FC<SelectProps> = ({
  placeholder,
  value = [],
  options = [],
  multiple = false,
  searchable = false,
  leftIcon,
  rightIcon,
  onChange,
  onSearchChange,
  asyncCallback,
}) => {
  const [selectedOptions, setSelectedOptions] = useState<SelectOptionType[]>(value);
  const [opts, setOpts] = useState(options);
  const [isFocused, setIsFocused] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [deleteMode, setDeleteMode] = useState(!multiple && searchable);
  const [loading, setLoading] = useState(false);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);

  useOnClickOutside(wrapperRef, () => {
    setIsFocused(false);
    inputRef.current?.blur();
  });

  const debouncedSearchText = useDebounce(searchText, 500);

  useEffect(() => {
    if (asyncCallback) {
      setLoading(true);
      asyncCallback(debouncedSearchText)
        .then((res) => {
          setOpts(res);
        })
        .finally(() => setLoading(false));
    }
  }, [debouncedSearchText, asyncCallback]);

  const fuse = new Fuse(opts, {
    keys: ['label'],
  });

  const filteredOpts = useMemo(() => {
    if (!searchText) return opts;
    const res = fuse.search(searchText);
    return res.length ? res.map((data) => data.item) : [];
  }, [fuse, searchText, opts]);

  const mappedValueSelectedOpt = useMemo(
    () => selectedOptions.map((data) => data.value),
    [selectedOptions],
  );

  const handleSearch = useCallback(
    (val: string) => {
      setSearchText(val);
      onSearchChange && onSearchChange(val);
    },
    [setSearchText, onSearchChange, asyncCallback],
  );

  const handleOptionClick = useCallback(
    (data: SelectOptionType) => {
      if (data.value !== '' && !mappedValueSelectedOpt.includes(data.value)) {
        setSelectedOptions((prev) => {
          const temp = multiple ? [...prev, data] : [data];
          onChange(temp);
          return temp;
        });

        if (!multiple) {
          setIsFocused(false);
          handleSearch(data.label);
        }
      }
    },
    [
      mappedValueSelectedOpt,
      setSelectedOptions,
      setIsFocused,
      handleSearch,
      multiple,
      onChange,
    ],
  );

  const handleKeyUpInput = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      switch (e.key) {
        case 'Tab':
          !isFocused && setIsFocused(true);
          break;
        case 'Backspace':
          if (!deleteMode) {
            setDeleteMode(true);
          } else if (deleteMode) {
            setSelectedOptions((prev) => {
              const temp = prev.slice(0, prev.length - 1);
              onChange(temp);
              return temp;
            });
          }
          break;
        default:
          if (deleteMode && multiple && searchable) setDeleteMode(false);
          break;
      }
    },
    [
      searchText,
      isFocused,
      setIsFocused,
      deleteMode,
      setDeleteMode,
      setSelectedOptions,
      onChange,
    ],
  );

  const handleDeleteSelectedOpts = useCallback(
    (data: SelectOptionType) => {
      setSelectedOptions((prev) => {
        const temp = prev.filter((opt) => opt.value !== data.value);
        onChange(temp);
        return temp;
      });
    },
    [onChange],
  );

  const handleKeyDownOption = useCallback((e) => {
    if (e.key === 'ArrowUp') {
      e.currentTarget.previousSibling.focus();
    } else if (e.key === 'ArrowDown') {
      e.currentTarget.nextSibling.focus();
    }
  }, []);

  const selectIcon = useMemo(() => {
    if (loading) return <LoadingIcon />;
    return isFocused ? <CollapseIcon /> : <ExpandIcon />;
  }, [isFocused, loading]);

  const tagRender = useMemo(
    () =>
      selectedOptions.map((opt, index) => (
        <Tag key={index} onClick={() => handleDeleteSelectedOpts(opt)}>
          {opt.label} <CloseIcon />
        </Tag>
      )),
    [selectedOptions, handleDeleteSelectedOpts],
  );

  return (
    <Wrapper ref={wrapperRef}>
      <Container
        css={[
          isFocused && tws`border-primary-500`,
          searchable && multiple && tws`flex-col`,
          searchable ? tws`cursor-text` : tws`cursor-pointer`,
        ]}
        onClick={() => {
          setIsFocused(true);
          if (searchable) {
            inputRef.current?.focus();
          }
        }}
      >
        {multiple &&
          Boolean(selectedOptions.length) &&
          (searchable ? (
            <TagContainer>{tagRender}</TagContainer>
          ) : (
            <React.Fragment>{tagRender}</React.Fragment>
          ))}
        <SelectInputContainer
          css={[
            searchable &&
              multiple &&
              Boolean(selectedOptions.length) &&
              tws`border-t pt-1`,
          ]}
        >
          {leftIcon}
          <SelectInput
            ref={inputRef}
            value={
              !multiple && !searchable && selectedOptions.length
                ? selectedOptions[0].label
                : searchText
            }
            readOnly={!searchable}
            placeholder={placeholder ?? 'Select options...'}
            onChange={(e) => handleSearch(e.target.value)}
            onKeyUp={handleKeyUpInput}
            style={{ cursor: 'inherit' }}
          />
          {rightIcon ?? selectIcon}
        </SelectInputContainer>
      </Container>
      {isFocused && (
        <SelectContainer>
          {filteredOpts.map((data, index) => (
            <SelectOption
              css={[mappedValueSelectedOpt.includes(data.value) && tws`bg-primary-100`]}
              key={index}
              onClick={() => handleOptionClick(data)}
              onKeyDown={handleKeyDownOption}
            >
              {data.label}
            </SelectOption>
          ))}
          {!filteredOpts.length && <SelectOption tabIndex={-1}>No Options.</SelectOption>}
        </SelectContainer>
      )}
    </Wrapper>
  );
};

export default Select;
