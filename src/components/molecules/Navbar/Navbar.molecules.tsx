// #region IMPORTS
import React from 'react';
import { Menu2 } from 'tabler-icons-react';

import { Text } from '@/components/atoms';
import { CssStyle, styled, tw } from '@/utils/windicss';
// #endregion IMPORTS

// #region STYLED COMPONENTS
const Container = tw.div`bg-white py-4 px-8 rounded-full border border-slate-10 shadow-md flex justify-between items-center`;
const HamburgerMenu = styled(
  Menu2,
)`w-10 h-10 p-2 rounded-full hover:(bg-slate-100) cursor-pointer transition-all`;
// #endregion STYLED COMPONENTS

// #region PROPS
type NavbarProps = {
  extraContainerStyle?: CssStyle;
};
// #endregion PROPS

const Navbar: React.FC<NavbarProps> = ({ extraContainerStyle }) => {
  return (
    <Container css={extraContainerStyle}>
      <Text.SmallFit>AS</Text.SmallFit>
      <HamburgerMenu />
    </Container>
  );
};

export default Navbar;
