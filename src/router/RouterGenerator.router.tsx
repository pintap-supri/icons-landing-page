//#region IMPORTS

import React from 'react';
import { Redirect, Route } from 'wouter';

import { AppRoute } from './Types.router';

//#endregion

//#region ROUTE

export function RouterGenerator(
  routes: AppRoute[],
  isAuthenticated: boolean,
  defaultRoute: string,
): JSX.Element[] {
  return routes.map(({ name, path, RouteComponent, isPrivate }, index) => {
    if (typeof RouteComponent === 'undefined') return <></>;

    return (
      <Route key={`${name}-${index}`} path={path}>
        {(params) =>
          !isPrivate || isAuthenticated ? (
            <RouteComponent params={params} />
          ) : (
            <Redirect to={defaultRoute} />
          )
        }
      </Route>
    );
  });
}

//#endregion
