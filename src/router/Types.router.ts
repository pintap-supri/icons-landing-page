//#region IMPORTS

import React from 'react';
import { RouteComponentProps } from 'wouter';

//#endregion

//#region TYPINGS

export type AppRoute = {
  path: string;
  name: string;
  isPrivate: boolean;
  exact?: boolean;
  RouteComponent?: React.LazyExoticComponent<React.FC<RouteComponentProps>>;
};

//#endregion
