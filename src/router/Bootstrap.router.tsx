//#region IMPORTS

import { observer } from 'mobx-react';
import React, { useCallback } from 'react';
import { Switch } from 'wouter';

import { Routes } from '../config/Router.config';
import { RouterGenerator } from './RouterGenerator.router';

//#endregion

//#region ROUTER

export const Router: React.FC = observer(() => {
  const isAuthenticated = false;

  //#region HELPER

  const generateRoute = useCallback(
    () => RouterGenerator(Routes, isAuthenticated, '/login'),
    [isAuthenticated],
  );

  //#endregion

  //#region RENDER

  return <Switch>{generateRoute()}</Switch>;

  //#endregion
});

//#endregion
