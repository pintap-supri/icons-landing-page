import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig, loadEnv } from 'vite';
import { chunkSplitPlugin } from 'vite-plugin-chunk-split';
import viteCompression from 'vite-plugin-compression';
import { VitePWA } from 'vite-plugin-pwa';
import WindiCSS from 'vite-plugin-windicss';
import tsconfigPaths from 'vite-tsconfig-paths';

export default ({ mode }: { mode: string }) => {
  const env = loadEnv(mode, process.cwd(), '');
  const processEnvValues = {
    'process.env': Object.entries(env).reduce((prev, [key, val]) => {
      return {
        ...prev,
        [key]: val,
      };
    }, {}),
  };

  return defineConfig({
    base: env.REACT_APP_BASE_PATH,
    server: {
      port: 3000,
    },
    build: {
      minify: mode === 'development' ? false : 'terser',
      outDir: './build',
    },
    esbuild: {
      jsxFactory: 'jsx',
      logOverride: { 'this-is-undefined-in-esm': 'silent' },
    },
    plugins: [
      react(),
      viteCompression({
        threshold: 512,
        ext: '.br',
        algorithm: 'brotliCompress',
        compressionOptions: {
          level: 10,
        },
      }),
      chunkSplitPlugin({
        strategy: 'single-vendor',
        customSplitting: {
          // `react` and `react-dom` will be bundled together in the `react-vendor` chunk (with their dependencies, such as object-assign)
          'react-vendor': ['react', 'react-dom', 'wouter'],
          lodash: ['lodash'],
          'table-vendor': [
            'date-fns',
            'emoji-picker-react',
            'react-base-table',
            'react-konva',
          ],
        },
      }),
      VitePWA(),
      tsconfigPaths(),
      WindiCSS(),
    ],
    resolve: {
      alias: [
        {
          find: /^@app\/(.*)$/,
          replacement: `${path.resolve('../../')}/packages/$1`,
        },
      ],
    },
    define: processEnvValues,
  });
};
